﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public GameObject Izquierda;
    public GameObject Derecha;
    public GameObject[] Objetos;
    public bool isOpen;
    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
    }

    public void open(){
        Izquierda.transform.position = new Vector2(Izquierda.transform.position.x - 1.5f, Izquierda.transform.position.y);
        Derecha.transform.position = new Vector2(Derecha.transform.position.x + 1.5f, Derecha.transform.position.y);
        isOpen = true;
            foreach (GameObject i in Objetos)
            {
                i.transform.position = new Vector3(i.transform.position.x,
                                                   i.transform.position.y,
                                                   -1);
            }
    }
    public void close(){
        Izquierda.transform.position = new Vector2(Izquierda.transform.position.x + 1.5f, Izquierda.transform.position.y);
        Derecha.transform.position = new Vector2(Derecha.transform.position.x - 1.5f, Derecha.transform.position.y);
        isOpen = false;
            foreach (GameObject i in Objetos)
            {
                i.transform.position = new Vector3(i.transform.position.x,
                                                   i.transform.position.y,
                                                   1);
            }
    }
}
