﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCursor : MonoBehaviour
{ 

    public Texture2D cursor;
    public Texture2D cursorMano;

    // Start is called before the first frame update
    void Start()
    {
    }

    void OnMouseOver()
    {
        Debug.Log("contacto");
        Cursor.SetCursor(cursorMano, Vector2.zero, CursorMode.ForceSoftware);
        
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.ForceSoftware);
    }
}
