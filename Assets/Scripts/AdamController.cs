﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AdamController : MonoBehaviour
{
    public float speed;
    Vector3 target;
    Animator anim;
    public bool walk;
    private float waitTime = 7f;
    private float timer = 0.0f;
    public bool talking;
    public bool picking;

    void Start()
    {
        transform.position = VariablesManager.playerPosition;
        target = transform.position;
        anim = this.gameObject.GetComponent<Animator>();
        walk = false;

    }

    void FixedUpdate()
    {
        timer += Time.deltaTime;
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (Input.GetMouseButton(0))
        {
            if (!talking)
            {
                timer = 0f;
                Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                target.x = mousePosition.x;
                target.y = mousePosition.y;
                Vector2 direction = new Vector2(
                    mousePosition.x - transform.position.x,
                    mousePosition.y - transform.position.y);
                transform.right = new Vector2(direction.x, 0f);
                walk = true;
            }
        }

        else
        {
            Vector2 v2transform = new Vector2(transform.position.x, transform.position.y);
            if (Mathf.Abs(Vector2.Distance(v2transform, target)) <= 0.5f)
            {
                walk = false;
            }
            
            else if (timer > waitTime)
            {
                timer = 0f;
                walk = false;
            }
        }



        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if (walk == true) { anim.SetBool("walking", true); }
        if (walk == false) { anim.SetBool("walking", false); }

        if (!picking)
        {
            anim.SetBool("cogiendo", false);
        }

        if (picking)
        {
            picking = false;
            anim.SetBool("cogiendo", true);
            //anim.SetBool("cogiendo", false);
        }
    }

}
