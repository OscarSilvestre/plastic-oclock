﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMainNPC", menuName = "NPCs/MainNPC")]
public class NPC : ScriptableObject
{
    //public Sprite image = null;
    new public string name = "New Main NPC";
    
    public Dialogue firstInteraction;
    public Dialogue secondInteraction;
    public Dialogue itemWrong;
    public Dialogue itemCorrect;
    public Dialogue puzzle1Completed;
    public Dialogue puzzle1CompletedSecondInteraction;
    public Dialogue puzzle2Completed;

    public NPC(string name)
    {
        this.name = name;
    }

    public NPC()
    {
       
    }
}
