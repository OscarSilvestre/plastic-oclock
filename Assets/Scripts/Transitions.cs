﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Transitions : MonoBehaviour
{
    public Animator anim;
    public Image Image;


    void Start()
    {
        StartCoroutine(LoadScene());
    }

    public IEnumerator LoadScene()
    {
        anim.SetBool("end", true);
        yield return new WaitForSeconds(1f);
        Image.enabled = false;//this.gameObject.SetActive(false);
    }

    public void fadeIn()
    {
        Image.enabled = true;// this.gameObject.SetActive(true);
        anim.SetBool("end", false);
    }

    public void fadeOut()
    {
        StartCoroutine(LoadScene());
    }
}
