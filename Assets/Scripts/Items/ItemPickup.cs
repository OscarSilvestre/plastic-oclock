using UnityEngine;
using UnityEngine.UI;

public class ItemPickup : MonoBehaviour {

	public Item item;   // Item to put in the inventory on pickup
	public bool picked;

	// Pick up the item
	public void PickUp ()
	{
		Debug.Log("Picking up " + item.name);
		bool wasPickedUp = Inventory.instance.Add(item);	// Add to inventory

		// If successfully picked up
		if (wasPickedUp)
        {
			picked = true;
			//Destroy(gameObject);    // Destroy item from scene
			//this.gameObject.SetActive(false);
		}
			
	}

}
