﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName ="NewItem", menuName = "Inventory/Item")]
public class Item : ScriptableObject {

    public Sprite image = null;
    new public string name = "New Item";

    public Item(Sprite image, string name)
    {
        this.image = image;
        this.name = name;

    }

    public Item()
    {

    }
    
}
