﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlots : MonoBehaviour
{
    public Image icon;
    Item objeto;

    public void AddItem(Item newobjeto)
    {
        objeto = newobjeto;

        icon.sprite = objeto.image;
        icon.enabled = true;
    }

    public void ClearSlot()
    {
        objeto = null;

        icon.sprite = null;
        icon.enabled = false;
    }

}
