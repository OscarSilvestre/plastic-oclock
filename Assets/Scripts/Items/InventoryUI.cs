﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    Inventory objetos;

    public Transform itemsParent;

    InventorySlots[] slots;
    
        
    
    private void Start()
    {
        objetos = Inventory.instance;
        objetos.onItemChangedCallback += UpdateUI;

        slots = itemsParent.GetComponentsInChildren<InventorySlots>();
    }  

    public void UpdateUI()
    {
        Debug.Log("UI actualizada");

        for ( int i =0; i<slots.Length; i++)
        {
            if( i < objetos.items.Count)
            {
                slots[i].AddItem(objetos.items[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }



}
