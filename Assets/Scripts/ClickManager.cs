﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ClickManager : MonoBehaviour
{
    public Animator animChangeScene;
    public Transitions transitions;

    public Animator animDialogue;
    public bool talking;

    public GameObject player;
    

    public static bool sceneChanged;
    void Start()
    {
        talking = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            Debug.Log("click");
            

            if (hit.collider != null)
            {
                if (!talking)
                { //El personaje no esta hablando con otros; es decir, se encuentra en facultades para andar
                    if (hit.collider.gameObject.tag == "NPC")
                    {

                        if (playerInRange(hit.collider.gameObject))
                        {
                            Debug.Log("golpeado");
                            animDialogue.SetBool("Is_Open", true);
                            NPCTalk talk = hit.collider.GetComponent<NPCTalk>();
                            player.GetComponent<AdamController>().talking = true;
                            player.GetComponent<AdamController>().walk = false;
                            talking = true;
                            talk.Talk();
                            
                        }
                    }

                    if (hit.collider.gameObject.tag == "BNPC")
                    {

                        if (playerInRange(hit.collider.gameObject))
                        {
                            Debug.Log("golpeado");
                            animDialogue.SetBool("Is_Open", true);
                            RandomNPCTalk talk = hit.collider.GetComponent<RandomNPCTalk>();
                            player.GetComponent<AdamController>().talking = true;
                            player.GetComponent<AdamController>().walk = false;
                            talking = true;
                            talk.RandomTalk();
                            
                        }
                    }

                    if (hit.collider.gameObject.tag == "Item") //Item
                    {
                        if (playerInRange(hit.collider.gameObject))
                        {
                            Debug.Log("cogido");
                            player.GetComponent<AdamController>().picking = true;
                            ItemPickup pick = hit.collider.GetComponent<ItemPickup>();
                            pick.PickUp();
                        }
                    }

                    if (hit.collider.gameObject.tag == "Exit") //Cambio de escena
                    {
                        if (playerInRange(hit.collider.gameObject))
                        {
                            Debug.Log("salida");
                            sceneChanged = true;
                            VariablesManager.playerPosition = PositionDictionary.changeScenePostions[new System.Tuple<string, string>(SceneManager.GetActiveScene().name, hit.collider.gameObject.name)];
                            StartCoroutine(LoadScene(hit.collider.gameObject.name));
                        }
                    }

                    if (hit.collider.gameObject.tag == "PuzzleTrigger") //Minijuego
                    {
                        Debug.Log("puzzle");
                        //GameObject puzzle = GameObject.Find("Note_Puzzle");//hit.collider.gameObject.name.Remove(0, 7));

                        StartCoroutine(LoadScene(hit.collider.gameObject.name.Remove(0, 8)));
                    }
                }
            }

            if (hit.collider == null)
            {
                Debug.Log("click nulo");
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);         

            if (hit.collider != null)
            {              

                if (hit.collider.gameObject.tag == "Door") //Puerta
                {
                    if (playerInRange(hit.collider.gameObject))
                    {
                        Debug.Log("Abrir");
                        Puerta door = hit.collider.GetComponent<Puerta>();
                        if(door.isOpen)
                            door.close();
                        else
                            door.open();
                    }
                }
            }
            else
                Debug.Log("click nulo");
        }
    }

    public IEnumerator LoadScene(string SceneName)
    {
        //animChangeScene.SetTrigger("end");
        transitions.fadeIn();
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene(SceneName);
    }

    bool playerInRange(GameObject go)
    {
        if (Vector2.Distance(player.transform.position, go.transform.position) <= 4f)
        {
            Debug.Log("en distancia");
            return true;
        }

        else
        {
            Debug.Log("no en distancia");
            return false;
        }
    }
}
