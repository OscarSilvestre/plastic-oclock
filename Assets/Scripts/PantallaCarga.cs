﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PantallaCarga : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        VariablesManager.playerPosition = PositionDictionary.changeScenePostions[new System.Tuple<string, string>(SceneManager.GetActiveScene().name, "Presente-Calle")];
        StartCoroutine(LoadScene("Presente-Calle"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator LoadScene(string SceneName)
    {
        //animChangeScene.SetTrigger("end");
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene(SceneName);
    }
}
