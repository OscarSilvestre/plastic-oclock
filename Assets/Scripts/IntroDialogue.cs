﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroDialogue : MonoBehaviour
{
    public Animator animator;
    public Dialogue dialogue; 
    public Text message;        //Texto Dialogo

    public AudioSource taca;

    private Queue<string> sentence2 = new Queue<string>();
    private Queue<string> sentenceWrong = new Queue<string>();

    void Start()
    {
        
        StartDialogue(dialogue);
    }

    public void StartDialogue(Dialogue dialogue)
    {
        //animator.SetBool("Is_Open", true);
        sentence2.Clear();
        foreach (string sentence in dialogue.senteces)
        {
            sentence2.Enqueue(sentence);
        }
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentence2.Count == 0)
        {
            message.text = "";
            taca.Stop();
            EndDialogue();
            return;
        }
        //taca.Play();
        string sentence = sentence2.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentece)
    {
        message.text = "";
        taca.Play();
        foreach (char letter in sentece.ToCharArray())
        {
            
            message.text += letter;
            yield return new WaitForSeconds(0.085f);//null;
        }
      
        taca.Stop();
    }

    void EndDialogue()
    {
        //animator.SetBool("Is_Open", false);
        if(SceneManager.GetActiveScene().name == "Intro")
        {
            StartCoroutine(LoadScene("Futuro-Exterior"));
        }

        if (SceneManager.GetActiveScene().name == "Outro")
        {
            /*GameObject.Find("EventsManager").GetComponent<EventsManager>().Reset();
            GameObject.Find("VariableManager").GetComponent<VariablesManager>().Reset();
            StartCoroutine(LoadScene("MainMenu"));*/
            Application.Quit();
        }

    }


    public void StarWrongtDialogue(Dialogue dialogue)
    {
        //animator.SetBool("Is_Open", true);

        foreach (string sentence in dialogue.senteces)
        {
            sentenceWrong.Enqueue(sentence);
        }

        DisplayNextSentenceWrong();
    }

    public void DisplayNextSentenceWrong()
    {
        if (sentenceWrong.Count == 0)
        {
            message.text = "";
            EndDialogue();
            return;
        }

        string sentence = sentenceWrong.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            DisplayNextSentence();
        }
    }

    public IEnumerator LoadScene(string SceneName)
    {
        //animChangeScene.SetTrigger("end");
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(SceneName);
        
    }
}

