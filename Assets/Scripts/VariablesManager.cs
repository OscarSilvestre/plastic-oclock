﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariablesManager : MonoBehaviour
{
    Inventory inventory;

    public static List<Item> itemList = new List<Item>();

    public static Vector3 playerPosition = new Vector3(-8f, -3f, 0);

    public Texture2D cursor;

    

    private void Start()
    {
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.ForceSoftware);
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateItemList;
    }

    public void UpdateItemList()
    {
        Debug.Log("lista items actualizada");
        itemList = inventory.items;
    }

    public void Reset()
    {
        playerPosition = new Vector3(-8f, -3f, 0);
    }
}
