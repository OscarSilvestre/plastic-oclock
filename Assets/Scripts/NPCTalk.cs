﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCTalk : MonoBehaviour
{
    public NPC npc;
    
    private bool talking;

    public DialogueManager dialogue;
    public bool talked1;
    public bool talked2;
    public bool puzzle1Com;
    public bool puzzle2Com;

    //public Text PjName;

    void Start()
    {
        
        talking = false;
        talked1 = false;
        talked2 = false;
        puzzle1Com = false;
        puzzle2Com = false;
    }

    public void Talk()
    {
        if (!talking)
        {
            if (talked1 == false && puzzle1Com == false)
            {
                Debug.Log("hablo");
                dialogue.StartDialogue(npc.firstInteraction);
                talked1 = true;
            }

            else if (talked1 == true && puzzle1Com == false)
            {
                dialogue.StartDialogue(npc.secondInteraction);
            }

            if (puzzle1Com == true && talked2 == false)
            {
                dialogue.StartDialogue(npc.puzzle1Completed);
                talked2 = true;
            }

            else if (puzzle2Com == false && talked2 == true)
            {
                dialogue.StartDialogue(npc.puzzle1CompletedSecondInteraction);
            }

            if (puzzle2Com == true)
            {
                dialogue.StartDialogue(npc.puzzle2Completed);
            }
        }
    }
}
