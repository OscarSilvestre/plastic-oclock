﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionDictionary : MonoBehaviour
{
    public static IDictionary<Tuple<string, string>, Vector3> changeScenePostions = new Dictionary<Tuple<string, string>, Vector3>();

    void Awake()
    {
        changeScenePostions.Add(new Tuple<string, string>("Futuro-Exterior", "Futuro-Interior"), new Vector3(-12.8f, -4.20f));
        changeScenePostions.Add(new Tuple<string, string>("Futuro-Interior", "Futuro-Exterior"), new Vector3(8f, -2.61f));
        changeScenePostions.Add(new Tuple<string, string>("Note_Puzzle", "Futuro-Interior"), new Vector3(-5f, -2.6f));
        changeScenePostions.Add(new Tuple<string, string>("Fuel_Puzzle", "Futuro-Interior"), new Vector3(-9.7f, -2.6f));

        changeScenePostions.Add(new Tuple<string, string>("Futuro-Interior", "PantallaCarga"), new Vector3(2.6f, -4.55f));
        changeScenePostions.Add(new Tuple<string, string>("PantallaCarga", "Presente-Calle"), new Vector3(2.6f, -4.55f));

        changeScenePostions.Add(new Tuple<string, string>("Presente-Calle", "Map_Puzzle"), new Vector3(-8.5f, -3.25f));
        changeScenePostions.Add(new Tuple<string, string>("Map_Puzzle", "Presente-Calle"), new Vector3(-8.9f, -4.8f));
        changeScenePostions.Add(new Tuple<string, string>("Map_Puzzle", "Presente-Universidad"), new Vector3(0f, -4.8f));
        changeScenePostions.Add(new Tuple<string, string>("Presente-Calle", "Presente-Universidad"), new Vector3(0f, -4.8f));
        changeScenePostions.Add(new Tuple<string, string>("Presente-Universidad", "Presente-Calle"), new Vector3(8.5f, -4.5f));
        changeScenePostions.Add(new Tuple<string, string>("Presente-Universidad", "Map_Puzzle1"), new Vector3(0.7f, 4.6f));
        changeScenePostions.Add(new Tuple<string, string>("Map_Puzzle1", "Presente-Universidad"), new Vector3(-8.8f, -4.8f));
        changeScenePostions.Add(new Tuple<string, string>("Map_Puzzle1", "Presente-Costa"), new Vector3(-21.4f, -3.2f));
        changeScenePostions.Add(new Tuple<string, string>("Presente-Universidad", "Presente-Costa"), new Vector3(-21.4f, -3.2f));
        changeScenePostions.Add(new Tuple<string, string>("Presente-Costa", "Presente-Universidad"), new Vector3(9f, -4.8f));
        changeScenePostions.Add(new Tuple<string, string>("Bottle_Puzzle", "Presente-Costa"), new Vector3(4.55f, -1.6f));

        changeScenePostions.Add(new Tuple<string, string>("Presente-Costa", "Outro"), new Vector3(9f, -4.8f));
    }

    
}
