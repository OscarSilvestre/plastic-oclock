﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Animator animator;
    public ClickManager CM;
    public AdamController player;
    public Image headimage;     //Imagen Cabeza Personaje
    public Text PjName;         //Texto Nombre Personaje
    public Text message;        //Texto Dialogo
    private Queue<string> names = new Queue<string>();
    private Queue<Sprite> sprites = new Queue<Sprite>();
    private Queue<string> sentence2 = new Queue<string>();
    private Queue<string> sentenceWrong = new Queue<string>();


    public void StartDialogue(Dialogue dialogue)
    {
        animator.SetBool("Is_Open", true);
        //dialogueBox.SetActive(true);
        //PjName.text = dialogue.namePJ;
        sentence2.Clear();
        foreach (string sentence in dialogue.senteces)
        {
            sentence2.Enqueue(sentence);
        }

        foreach (string name in dialogue.namePJs)
        {
            names.Enqueue(name);
        }

        foreach (Sprite sprite in dialogue.spritePJs)
        {
            sprites.Enqueue(sprite);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentence2.Count == 0)
        {
            message.text = "";
            EndDialogue();
            return;
        }

        string sentence = sentence2.Dequeue();
        string name = names.Dequeue();
        Sprite sprite = sprites.Dequeue();
        StopAllCoroutines();
        PjName.text = name;
        headimage.sprite = sprite;
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentece)
    {
        message.text = "";
        foreach (char letter in sentece.ToCharArray())
        {
            message.text += letter;
            yield return null;
        }
    }

    void EndDialogue()
    {
        animator.SetBool("Is_Open", false);
        CM.talking = false;
        player.talking = false;
        //dialogueBox.SetActive(false);

    }


    public void StarWrongtDialogue(Dialogue dialogue)
    {
        animator.SetBool("Is_Open", true);
        //dialogueBox.SetActive(true);


        foreach (string sentence in dialogue.senteces)
        {
            sentenceWrong.Enqueue(sentence);
        }

        DisplayNextSentenceWrong();
    }

    public void DisplayNextSentenceWrong()
    {
        if (sentenceWrong.Count == 0)
        {
            message.text = "";
            EndDialogue();
            return;
        }

        string sentence = sentenceWrong.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            DisplayNextSentence();
        }
    }
}

