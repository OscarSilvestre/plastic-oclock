﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePuzzleTrigger : MonoBehaviour
{
    public NPCTalk jason;
    public GameObject p1trigger;
    public GameObject p2trigger;


    public Animator anim;

    // Update is called once per frame
    void Update()
    {
       
        if (jason.talked1)
        {
            if (anim.GetBool("Is_Open") == false)
            {

                p1trigger.SetActive(true);

            }
        }

        if (EventsManager.jigsawCompleted)
        {
            p2trigger.GetComponent<Collider2D>().enabled = true;
        }
    }
}
