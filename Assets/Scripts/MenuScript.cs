﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public GameObject panel;
    public AudioSource pitido;

    public void Play()
    {
        pitido.Play();
        SceneManager.LoadScene("Intro");
    }

    public void Controls()
    {
        pitido.Play();
        if (panel.activeSelf == true) { panel.SetActive(false); }
        else { panel.SetActive(true); }
    }

    public void Quit()
    {
        pitido.Play();
        Application.Quit();
    }
}
