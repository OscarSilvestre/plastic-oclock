﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EventsManager : MonoBehaviour
{
    //public NPCTalk jason;
    //public GameObject ptrigger;
    //public JigsawPuzzle p;
    
    //public Animator anim;

    bool sceneChanged;
    public static bool jigsawCompleted;
    public static bool fuelCompleted;
    bool gadolinioPicked;
    bool praseodimioPicked;

    bool universityReached;
    bool beachReached;
    bool studentTalked;
    bool HawkingTalked;
    bool mapPuzzleEntered;

    bool adamTalked1;
    bool adamTalked2;
    public static bool bottleCompleted;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "Futuro-Exterior")
        {
            sceneChanged = ClickManager.sceneChanged;
            if (sceneChanged)
            {
                GameObject adam = GameObject.Find("Adam");
                Destroy(adam);
            }
        }

        if (SceneManager.GetActiveScene().name == "Futuro-Interior")
        {
            GameObject jason1 = GameObject.Find("Jason1");
            GameObject jason2 = GameObject.Find("Jason2");
            GameObject gadolinio = GameObject.Find("Gadolinio");
            GameObject praseodimio = GameObject.Find("Praseodimio");
            GameObject salida = GameObject.Find("PantallaCarga");

            if (jigsawCompleted == true)
            {
                jason1.GetComponent<NPCTalk>().puzzle1Com = true;
            }

            if (fuelCompleted)
            {
                jason1.GetComponent<Collider2D>().enabled = false;
                jason1.GetComponent<NPCTalk>().puzzle2Com = true;
                jason2.GetComponent<Collider2D>().enabled = true;
                foreach (Item item in Inventory.instance.items)
                {
                    if (item.name == "Gadolinio")
                    {
                        Inventory.instance.Remove(item);

                    }

                    if (item.name == "Praseodimio")
                    {
                        Inventory.instance.Remove(item);
                    }
                    salida.GetComponent<Collider2D>().enabled = true;
                }
                
            }

            if (!gadolinioPicked)
            {
                if (gadolinio.GetComponent<ItemPickup>().picked)
                {
                    gadolinioPicked = true;
                    Debug.Log("gadolinio cogido");
                    //gadolinio.SetActive(false);
                }
            }

            if (!praseodimioPicked)
            {
                if (praseodimio.GetComponent<ItemPickup>().picked)
                {
                    praseodimioPicked = true;
                    Debug.Log("praseodimio cogido");
                    //praseodimio.SetActive(false);
                }
            } 

            if (gadolinioPicked)
            {
                Debug.Log("g desactivado");
                //gadolinio.SetActive(false);
                Destroy(gadolinio);
            }
                
            if (praseodimioPicked)
            {
                Debug.Log("p desactivado");
                //praseodimio.SetActive(false);
                Destroy(praseodimio);
            }
                
        }

        if (SceneManager.GetActiveScene().name == "Presente-Calle")
        {
            GameObject estudiante = GameObject.Find("Estudiante");
            GameObject adam = GameObject.Find("Adam");
            GameObject salidapuzzle = GameObject.Find("Map_Puzzle");
            GameObject salida = GameObject.Find("Presente-Universidad");


            if (mapPuzzleEntered)
            {
                Destroy(estudiante);
                if (!universityReached)
                {
                    salidapuzzle.GetComponent<Collider2D>().enabled = true;
                    salidapuzzle.GetComponent<SpriteRenderer>().enabled = true;
                    adam.GetComponent<NPCTalk>().puzzle1Com = true;//cambiar dialogo adam a te has perdido?
                }
            }
            else
            {
                if (studentTalked)
                {
                    adam.GetComponent<NPCTalk>().talked1 = true;
                    salidapuzzle.GetComponent<Collider2D>().enabled = true;//SetActive(true);
                    salidapuzzle.GetComponent<SpriteRenderer>().enabled = true;
                }
                else
                {
                    if (estudiante.GetComponent<NPCTalk>().talked1)
                    {
                        studentTalked = true;
                    }
                }
            }

            if (universityReached)
            {
                salida.GetComponent<Collider2D>().enabled = true;
                salida.GetComponent<SpriteRenderer>().enabled = true;
                if (HawkingTalked)
                {
                    Destroy(adam);
                }
                else
                {
                    adam.GetComponent<NPCTalk>().puzzle2Com = true;//dialogo adam habla con mas gente
                }
            }
        }

        if (SceneManager.GetActiveScene().name == "Presente-Universidad")
        {
            universityReached = true;
            GameObject halconing = GameObject.Find("Halconing");
            GameObject salidapuzzle = GameObject.Find("Map_Puzzle1");
            GameObject salida = GameObject.Find("Presente-Costa");

            if (!HawkingTalked)
            {
                if (halconing.GetComponent<NPCTalk>().talked1)
                {
                    HawkingTalked = true;
                    salidapuzzle.GetComponent<Collider2D>().enabled = true;
                    salidapuzzle.GetComponent<SpriteRenderer>().enabled = true;
                }
            }
            else if(HawkingTalked && !beachReached)
            {
                halconing.GetComponent<NPCTalk>().talked1 = true;
                salidapuzzle.GetComponent<Collider2D>().enabled = true;
                salidapuzzle.GetComponent<SpriteRenderer>().enabled = true;
            }
            else if(HawkingTalked && beachReached)
            {
                halconing.GetComponent<NPCTalk>().talked1 = true;
                salida.GetComponent<Collider2D>().enabled = true;
                salida.GetComponent<SpriteRenderer>().enabled = true;
            }

            /*if (beachReached)
            {
                salida.SetActive(true);
            }*/
        }

        if (SceneManager.GetActiveScene().name == "Map_Puzzle")
        {
            mapPuzzleEntered = true;
        }

        if (SceneManager.GetActiveScene().name == "Presente-Costa")
        {
            beachReached = true;
            GameObject adam1 = GameObject.Find("Adam1");
            GameObject adam2 = GameObject.Find("Adam2");
            GameObject salida = GameObject.Find("Outro");
            GameObject tienda = GameObject.Find("Tienda-Campaña");
            GameObject ptrigger = GameObject.Find("Trigger_Bottle_Puzzle");
            GameObject cortinilla = GameObject.Find("Panel_T");
            GameObject tm = GameObject.Find("TimeMachine");
            GameObject DM = GameObject.Find("DialogueBox");

            if (!adam1.GetComponent<NPCTalk>().puzzle1Com)
            {
                if (adamTalked1)
                {
                    if (!DM.GetComponent<Animator>().GetBool("Is_Open")) //esperar a que termine la conversacion
                    {

                        cortinilla.GetComponent<Transitions>().fadeIn();//activar cortinilla
                        tm.GetComponent<SpriteRenderer>().enabled = true;
                        tm.GetComponent<Collider2D>().enabled = true;
                        tienda.GetComponent<SpriteRenderer>().enabled = true;
                        tienda.GetComponent<Collider2D>().enabled = true;

                        adam1.GetComponent<NPCTalk>().puzzle1Com = true;
                        cortinilla.GetComponent<Transitions>().fadeOut();//cortinilla.GetComponent<Animator>().SetBool("end", true);
                                                                         //cortinilla.GetComponent<Image>().enabled = false;
                    }
                }
            }

            if (adam1.GetComponent<NPCTalk>().talked1)
            {
                adamTalked1 = true;
            }

            

            if (adam1.GetComponent<NPCTalk>().talked2)
            {
                adamTalked2 = true;
            }

            if (adamTalked2)
            {
                ptrigger.GetComponent<Collider2D>().enabled = true;
            }

            if (bottleCompleted)
            {
                adam1.GetComponent<Collider2D>().enabled = false;
                adam1.GetComponent<NPCTalk>().puzzle2Com = true;
                adam2.GetComponent<Collider2D>().enabled = true;
                tm.GetComponent<Collider2D>().enabled = false;
                salida.GetComponent<Collider2D>().enabled = true;
            }

        }

    }

    public void Reset()
    {
        sceneChanged = false;
        jigsawCompleted = false;
        fuelCompleted = false;
        gadolinioPicked = false;
        praseodimioPicked = false;

        universityReached = false;
        beachReached = false;
        studentTalked = false;
        HawkingTalked = false;
        mapPuzzleEntered = false;

        adamTalked1 = false;
        adamTalked2 = false;
        bottleCompleted = false;
    }

    
}
