﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewBackgroundNPC", menuName = "NPCs/Background NPC")]
public class BackgroundNPC : ScriptableObject
{
    //public Sprite image = null;
    new public string name = "New Background NPC";
    
    //public Dialogue firstInteraction;
    //public Dialogue secondInteraction;
    /*public Dialogue thirdInteraction;
    public Dialogue forthInteraction;
    public Dialogue fifthInteraction;
    public Dialogue itemWrong;
    public Dialogue itemCorrect;*/
    public Dialogue smallTalk1;
    public Dialogue smallTalk2;
    public Dialogue smallTalk3;
    public Dialogue smallTalk4;
    public Dialogue smallTalk5;
    //public Dialogue puzzleCompleted;

    public BackgroundNPC(string name)
    {
        this.name = name;
    }

    public BackgroundNPC()
    {
       
    }
}
