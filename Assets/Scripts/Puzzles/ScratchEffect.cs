﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScratchEffect : MonoBehaviour
{
    public GameObject maskprefab;
    int count;
    int nombrecount;
    Vector2 lastposition;

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        lastposition = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (nombrecount < 15)
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
                RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

                if (hit.collider != null)
                {
                    if (Vector2.Distance(lastposition, mousePos2D) >= 0.5f)
                    {
                        GameObject maskSprite = Instantiate(maskprefab, mousePos2D, Quaternion.identity);
                        maskSprite.transform.parent = gameObject.transform;
                        lastposition = mousePos2D;
                        count++;
                        if(hit.collider.gameObject.tag == "Puzzle")
                        {
                            nombrecount++;
                            Debug.Log(nombrecount);
                        }
                        //Debug.Log(count);
                    }
                }
            }
        }
        else
        {
            Debug.Log("terminado");
            EventsManager.bottleCompleted = true;
            VariablesManager.playerPosition = PositionDictionary.changeScenePostions[new System.Tuple<string, string>(SceneManager.GetActiveScene().name, "Presente-Costa")];
            StartCoroutine(LoadScene("Presente-Costa"));
        }
    }

    public IEnumerator LoadScene(string SceneName)
    {
        
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(SceneName);
    }
}
