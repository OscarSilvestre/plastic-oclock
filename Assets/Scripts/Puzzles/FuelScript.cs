﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FuelScript : MonoBehaviour
{
    string colorClicked;
    string actualColor;
    public Sprite spriteAzul;
    public Sprite spriteRojo;
    public Sprite spriteAmarillo;
    public Sprite spriteVerde;
    public Sprite spriteNaranja;
    public Sprite spriteMorado;
    public Sprite spriteMarron;
    public Sprite spriteNegro;
    public Sprite spriteVacio;
    public SpriteRenderer SR;
    public GameObject mineral1;
    public GameObject mineral2;
    public GameObject mineralPuerta1;
    public GameObject mineralPuerta2;
    public GameObject fondoPuerta;
    public GameObject probetas;
    public GameObject boteRojo;
    public GameObject boteAzul;
    public GameObject boteAmarillo;
    public GameObject boteVacio;
    public GameObject colliderPuerta;
    public GameObject boton;
    public GameObject panelFaltanObj;

    public AudioSource puerta;
    public AudioSource probetasInterior;
    public AudioSource liquido;

    bool Gcogido;
    bool Pcogido;
    // Start is called before the first frame update
    void Start()
    {
        actualColor = "Empty";

        foreach (Item item in VariablesManager.itemList)
        {
            if(item.name == "Gadolinio")
            {
                Gcogido = true;
                mineral1.SetActive(true);

            }

            if(item.name == "Praseodimio")
            {
                Pcogido = true;
                mineral2.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Gcogido && Pcogido)
        {

            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
                RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
                Debug.Log("click");


                switch (hit.collider.gameObject.tag)
                {
                    case ("Rojo"):
                        Debug.Log("click Rojo.");
                        liquido.Play();
                        colorClicked = "Rojo";
                        ChangeColor();
                        break;

                    case ("Azul"):
                        Debug.Log("click Azul.");
                        liquido.Play();
                        colorClicked = "Azul";
                        ChangeColor();
                        break;

                    case ("Amarillo"):
                        Debug.Log("click Amarillo.");
                        liquido.Play();
                        colorClicked = "Amarillo";
                        ChangeColor();
                        break;

                    case ("Mineral1"):
                        Debug.Log("Click Mineral");
                        mineral1.SetActive(false);
                        mineralPuerta1.SetActive(true);
                        break;

                    case ("Mineral2"):
                        Debug.Log("Click Mineral");
                        mineral2.SetActive(false);
                        mineralPuerta2.SetActive(true);
                        break;

                    case ("PuertaMaquina"):
                        if (mineralPuerta1.activeSelf == true && mineralPuerta2.activeSelf == true)
                        {
                            puerta.Play();
                            mineralPuerta1.SetActive(false);
                            mineralPuerta2.SetActive(false);
                            fondoPuerta.SetActive(false);
                            StartCoroutine(Wait3Seconds());

                        }
                        break;

                    case ("Item"):
                        probetasInterior.Play();
                        
                        boteAmarillo.SetActive(true);
                        boteAzul.SetActive(true);
                        boteRojo.SetActive(true);
                        boteVacio.SetActive(true);
                        boton.SetActive(true);
                        probetas.SetActive(false);
                        break;

                }

            }

            if (actualColor == "Marron")
            {
                Debug.Log("Completado");
                VariablesManager.playerPosition = PositionDictionary.changeScenePostions[new System.Tuple<string, string>(SceneManager.GetActiveScene().name, "Futuro-Interior")];
                EventsManager.fuelCompleted = true;
                StartCoroutine(WaitSeconds());
            }
        }

        else
        {
            panelFaltanObj.SetActive(true);
            StartCoroutine(WaitSeconds());

        }

    }

    void ChangeColor()
    {   

        switch(actualColor)
        {
            // { CUANDO LA PROBETA ESTÁ VACÍA }

            case "Empty" when colorClicked == "Rojo":
                actualColor = "Rojo";
                SR.sprite = spriteRojo;
                colorClicked = "";
                break;

            case "Empty" when colorClicked == "Amarillo":
                actualColor = "Amarillo";
                SR.sprite = spriteAmarillo;
                colorClicked = "";
                break;

            case "Empty" when colorClicked == "Azul":
                actualColor = "Azul";
                SR.sprite = spriteAzul;
                colorClicked = "";
                break;

            // { CUANDO LA PROBETA TIENE UN COLOR }

            case "Rojo" when colorClicked == "Azul":
                actualColor = "Morado";
                SR.sprite = spriteMorado;
                colorClicked = "";
                break;

            case "Rojo" when colorClicked == "Amarillo":
                actualColor = "Naranja";
                SR.sprite = spriteNaranja;
                colorClicked = "";
                break;

            case "Rojo" when colorClicked == "Rojo":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;

            case "Azul" when colorClicked == "Rojo":
                actualColor = "Morado";
                SR.sprite = spriteMorado;
                colorClicked = "";
                break;

            case "Azul" when colorClicked == "Amarillo":
                actualColor = "Verde";
                SR.sprite = spriteVerde;
                colorClicked = "";
                break;

            case "Azul" when colorClicked == "Azul":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;

            case "Amarillo" when colorClicked == "Rojo":
                actualColor = "Naranja";
                SR.sprite = spriteNaranja;
                colorClicked = "";
                break;

            case "Amarillo" when colorClicked == "Azul":
                actualColor = "Verde";
                SR.sprite = spriteVerde;
                colorClicked = "";
                break;

            case "Amarillo" when colorClicked == "Amarillo":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;

            // { CUANDO LA PROBETA YA TIENE UNA MEZCLA }

            case "Verde" when colorClicked == "Rojo": //
                actualColor = "Marron";
                SR.sprite = spriteMarron;
                colorClicked = "";
                break;

            case "Verde" when colorClicked == "Amarillo":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;

            case "Verde" when colorClicked == "Azul":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;

            case "Morado" when colorClicked == "Amarillo": //
                actualColor = "Marron";
                SR.sprite = spriteMarron;
                colorClicked = "";
                break;

            case "Morado" when colorClicked == "Rojo":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;

            case "Morado" when colorClicked == "Azul":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;

            case "Naranja" when colorClicked == "Azul": //
                actualColor = "Marron";
                SR.sprite = spriteMarron;
                colorClicked = "";
                break;

            case "Naranja" when colorClicked == "Amarillo":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;

            case "Naranja" when colorClicked == "Rojo":
                actualColor = "Negro";
                SR.sprite = spriteNegro;
                colorClicked = "";
                break;
        }
    }

    IEnumerator WaitSeconds()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Futuro-Interior");
    }

    IEnumerator Wait3Seconds()
    {
        yield return new WaitForSeconds(5);
        fondoPuerta.SetActive(true);
        colliderPuerta.SetActive(false);
        probetas.SetActive(true);
    }

    public void Vaciar()
    {
        SR.sprite = spriteVacio;
        actualColor = "Empty";
    }

}
