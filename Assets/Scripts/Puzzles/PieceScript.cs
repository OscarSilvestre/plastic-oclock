﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class PieceScript : MonoBehaviour
{
    private Vector3 RightPosition;
    private Vector3 RightRotation;
    public bool InRightPosition;
    public bool InRightRotation;
    public bool Selected;
    public bool movable = true;

    public Collider2D col;
    void Start()
    {
        RightPosition = transform.position;
        RightRotation = new Vector3(0, 0, 0);//transform.eulerAngles;
        transform.eulerAngles = new Vector3(0, 0, (int)Mathf.Round(Random.Range(0, 3))*90);
        transform.position = new Vector3(Random.Range(5f, 6f), Random.Range(2f, -2f));
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, RightPosition) < 0.5f)
        {
            //if (!Selected)
            //{
                if (InRightPosition == false)
                {
                    transform.position = RightPosition;
                    InRightPosition = true;
                    movable = false;

                    if (InRightRotation == true && InRightPosition == true)
                    {
                        GetComponent<SortingGroup>().sortingOrder = 0;
                        Camera.main.GetComponent<JigsawPuzzle>().PlacedPieces++;
                        //col.enabled = false;
                    }
                }
            //}
        }

        if (transform.eulerAngles == RightRotation)
        {
            if (InRightRotation == false)
            {
                InRightRotation = true;
                if (InRightRotation == true && InRightPosition == true)
                {
                    GetComponent<SortingGroup>().sortingOrder = 0;
                    Camera.main.GetComponent<JigsawPuzzle>().PlacedPieces++;
                    //col.enabled = false;
                }
            }
            
        }

        
    }

    
}
