﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
public class JigsawPuzzle : MonoBehaviour
{
    public GameObject SelectedPiece;
    int OIL = 1;    
    public int PlacedPieces = 0;

    public bool completed;

    //public GameObject turnbutton;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider.tag == "Puzzle")
            {
                if (!hit.transform.GetComponent<PieceScript>().InRightPosition) //|| !SelectedPiece.transform.GetComponent<PieceScript>().InRightRotation)
                {
                    SelectedPiece = hit.transform.gameObject;
                    SelectedPiece.GetComponent<PieceScript>().Selected = true;
                    Debug.Log(SelectedPiece.GetComponent<PieceScript>().Selected);
                    Debug.Log(SelectedPiece.transform.GetComponent<PieceScript>().InRightRotation);
                    SelectedPiece.GetComponent<SortingGroup>().sortingOrder = OIL;
                    OIL++;
                }
                if (!hit.transform.GetComponent<PieceScript>().InRightRotation) //|| !SelectedPiece.transform.GetComponent<PieceScript>().InRightRotation)
                {
                    SelectedPiece = hit.transform.gameObject;
                    SelectedPiece.GetComponent<PieceScript>().Selected = true;
                    Debug.Log(SelectedPiece.GetComponent<PieceScript>().Selected);
                    Debug.Log(SelectedPiece.transform.GetComponent<PieceScript>().InRightRotation);
                    SelectedPiece.GetComponent<SortingGroup>().sortingOrder = OIL;
                    OIL++;
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (SelectedPiece != null)
            {
                SelectedPiece.GetComponent<PieceScript>().Selected = false;
                //SelectedPiece = null;
            }
        }
        if (SelectedPiece != null)
        {
            if(SelectedPiece.GetComponent<PieceScript>().Selected == true && SelectedPiece.GetComponent<PieceScript>().movable == true)
            {
                Vector3 MousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                SelectedPiece.transform.position = new Vector3(MousePoint.x, MousePoint.y, 0);
            }
            
        }             
        if (PlacedPieces == 8)
        {
            completed = true;
            VariablesManager.playerPosition = PositionDictionary.changeScenePostions[new System.Tuple<string, string>(SceneManager.GetActiveScene().name, "Futuro-Interior")];
            EventsManager.jigsawCompleted = true;
            StartCoroutine(WaitSeconds());
            
            
        }
    }

    public void rotate90Left()
    {
        if (!SelectedPiece.transform.GetComponent<PieceScript>().InRightRotation)
        {
            SelectedPiece.transform.eulerAngles += new Vector3(0, 0, 90);
        }
            
        //Debug.Log(SelectedPiece.transform.eulerAngles);
    }

    public void rotate90Right()
    {
        if (!SelectedPiece.transform.GetComponent<PieceScript>().InRightRotation)
        {
            SelectedPiece.transform.eulerAngles += new Vector3(0, 0, -90);
            
        }
        //Debug.Log(SelectedPiece.transform.eulerAngles);
            
    }

    IEnumerator WaitSeconds()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Futuro-Interior");
    }
}