﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapController : MonoBehaviour
{
    //public float speed;
    //Vector3 target;
    
    void Start()
    {
        //target = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector2 position = transform.position;
        position.x = position.x + 2.5f * horizontal * Time.deltaTime;
        position.y = position.y + 2.5f * vertical * Time.deltaTime;
        transform.position = position;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Rojo") { 
            Debug.Log("Salida mala.");
            VariablesManager.playerPosition = PositionDictionary.changeScenePostions[new System.Tuple<string, string>(SceneManager.GetActiveScene().name, other.gameObject.name)];
            SceneManager.LoadScene(other.gameObject.name);
        }

        if (other.gameObject.tag == "Verde") { 
            Debug.Log("Salida buena.");
            VariablesManager.playerPosition = PositionDictionary.changeScenePostions[new System.Tuple<string, string>(SceneManager.GetActiveScene().name, other.gameObject.name)];
            SceneManager.LoadScene(other.gameObject.name);
        }
    }
}
